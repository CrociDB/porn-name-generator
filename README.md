Porn Name Generator
======================

I've called this project a "porn name generator" because it's first pourpose was to create a "Porn Movie Name Generator App". But it may be used for other names as well.

Usage
----------------------

You'll need to feed the generator with a `data object`. It should be just like this:

	var data = {
		'cat': [ 'Put', 'Here', 'Some', 'Categories' ],
		'pre': [
			{ 'article': 'The', 'main': 'Something', 'categories': [0, 2], 'porn': false },
			{ 'article': 'Another', 'main': 'Shit', 'categories': 'all', 'porn': true }
		],
		'pos': [
			{ 'main': 'dos Anais', 'categories': 'all', 'porn': true },
		]
	};

I must say that the `'cat'` member in `data` won't be actually used by the generator.

Now prepare the generator:

	porngen = new PornGen;
	porngen.setData(data);

And then voila:

	name = porngen.getRandom([0, 2, 3]);

The `getRandom` method accepts an array of categories.
