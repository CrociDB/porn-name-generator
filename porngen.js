/**
 * Copyright (C) 2013 Bruno Croci
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
  * Why not NameGen?
  */
var PornGen = function() 
{
	this.setData = function (d) 
	{
		this.data = d;
	};

	this.getRandom = function (opt)
	{ 
		var pre = this.getRandomPreThing(opt);
		var pos = this.getRandomPosThing(opt, pre);

		var finalName = pre['main'] + ' ' + pos['main'];
		if (Math.random() > 0.34) finalName = pre['article'] + ' ' + finalName;

		return finalName;
	};

	this.getRandomPreThing = function (opt)
	{
		var randArray = [];
		for (var i in this.data['pre'])
		{
			var obj = this.data['pre'][i];
			if (obj['categories'] == 'all' || this.categoriesMatch(opt, obj['categories']))
			{
				randArray.push(obj);
			}
		}

		return randArray[Math.floor(Math.random() * randArray.length)];
	};

	this.getRandomPosThing = function (opt, pre)
	{
		var randArray = [];
		for (var i in this.data['pos'])
		{
			var obj = this.data['pos'][i];
			if (obj['porn'] != pre['porn'])
			{
				if (obj['categories'] == 'all' || this.categoriesMatch(opt, obj['categories']))
				{
					randArray.push(obj);
				}
			}
		}

		return randArray[Math.floor(Math.random() * randArray.length)];
	};

	this.categoriesMatch = function (opt, categories)
	{
		for (var i in opt)
		{
			for (var j in categories)
			{
				if (opt[i] == categories[j]) return true;
			}
		}

		return false;
	};
}
